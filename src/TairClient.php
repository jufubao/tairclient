<?php
declare(strict_types=1);

namespace TairClient;

use TairClient\Client\TairSearch;
use TairClient\Client\TairDoc;
use Hyperf\Redis\Redis;
use TairClient\Client\TairTs;

class TairClient {

    private TairSearch $tairSearch;
    private TairDoc $tairDoc;
    private TairTs $tairTs;
    private Redis $redis;

    public function __construct(Redis $client)
    {
        $this->tairSearch = new TairSearch($client);
        $this->tairDoc = new TairDoc($client);
        $this->tairTs = new TairTs($client);
        $this->redis = $client;
    }

    public function redis(): Redis {
        return $this->redis;
    }

    public function search(): TairSearch {
        return $this->tairSearch;
    }

    public function doc(): TairDoc {
        return $this->tairDoc;
    }

    /**
     * TairTS 时序数据结构，
     */
    public function ts(): TairTs {
        return $this->tairTs;
    }
}