<?php

declare(strict_types=1);
/**
 * 策略
 *
 * 自动审核商品
 *
 */
namespace TairClient\Command;

use Hyperf\Command\Annotation\Command;
use Hyperf\Command\Command as HyperfCommand;
use Hyperf\Utils\Codec\Json;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Input\InputArgument;
use TairClient\BaseModel\TairSearchBase;

#[Command]
class CreateIndexCommand extends HyperfCommand
{

    /**
     * @var ContainerInterface
     */
    protected $container;


    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        parent::__construct('tair:search:create-index');
    }

    public function configure()
    {
        parent::configure();
        $this->addArgument('index', InputArgument::REQUIRED, '索引文件类名');
    }

    private function instanceIndexClass() {
        $index = $this->input->getArgument('index');
        $classname = sprintf('\App\Database\Redis\TairSearch\%s', $index);
        if (!class_exists($classname)) {
            $this->error(sprintf('此索引文件类不存在 %s', $classname));
            exit(-1);
        }
        return new $classname();
    }

    public function handle()
    {

        /* @var $instance TairSearchBase */
        $instance = $this->instanceIndexClass();

        $file = sprintf('%s/app/Database/Redis/TairSearch/index/%s.json', BASE_PATH, $instance->getInitIndex());

        // 索引文件数据
        $indexStructJson = file_get_contents($file);
        $indexStructArray = Json::decode($indexStructJson);

        // 索引名
        $indexName = $instance->getIndex();

        $this->info(sprintf('索引名: %s', $indexName));

        // 检查索引是否存在
//        try {
//            $indexExist = $instance->getClient()->createIndex($indexName, $indexStructArray);
//
//            if ($indexExist) {
//                $this->error('此版本索引文件已经存在', [
//                    'index' => $indexExist,
//                ]);
//                return $this->exitError();
//            }
//
//        } catch (\Throwable $exception) {
//            $this->error('此索引已经存在');
//            return $this->exitError();
//        }

        // 创建新版本索引文件
        $instance->getClient()->createIndex($indexName, $indexStructArray);
        $this->info('创建索引成功');

        $this->line('完成', 'info');
    }
}
