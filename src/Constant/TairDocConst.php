<?php

declare(strict_types=1);

namespace TairClient\Constant;

class TairDocConst
{
    // 当path不存在时写入。
    const NX = 'NX';

    // 当path存在时写入。
    const XX = 'XX';
}