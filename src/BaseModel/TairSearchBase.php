<?php
declare(strict_types=1);

namespace TairClient\BaseModel;

use Hyperf\Utils\ApplicationContext;
use TairClient\Client\TairSearch;
use TairClient\TairClient;
use TairClient\TairFactory;

class TairSearchBase {

    // 实例
    protected string $instance = '';

    // 索引
    protected string $index = '';

    // 初始索引
    private string $initIndex = '';

    // client
    protected TairClient $client;

    public function __construct()
    {
        $container = ApplicationContext::getContainer();
        $this->client = $container->get(TairFactory::class)->get($this->instance);
        // 解析index
        $this->initIndex = $this->index;
        $this->index = $this->parserIndex();
    }

    /**
     * 解析index
     */
    private function parserIndex(): string {
        $prefix = Config(sprintf('redis.%s.prefix', $this->instance), Config('app_name'));
        return sprintf(
            '%s:tairsearch:%s',
            $prefix,
            $this->index
        );
    }

    public function getClient(): TairClient {
        return $this->client;
    }

    public function getInitIndex(): string {
        return $this->initIndex;
    }

    public function getIndex(): string {
        return $this->index;
    }
}