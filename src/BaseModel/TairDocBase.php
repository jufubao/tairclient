<?php
declare(strict_types=1);

namespace TairClient\BaseModel;

use Hyperf\Utils\ApplicationContext;
use TairClient\TairClient;
use TairClient\TairFactory;

class TairDocBase {

    // 实例
    protected string $instance = '';

    // key
    protected string $key = '';

    // client
    protected TairClient $client;

    public function __construct(array $args = [])
    {
        $container = ApplicationContext::getContainer();
        $this->client = $container->get(TairFactory::class)->get($this->instance);
        // 解析KEY
        $this->key = $this->parserKey();

        if (!empty($args)) {
            $this->key = sprintf($this->key, ...$args);
        }
    }

    /**
     * 解析key
     */
    private function parserKey(): string {
        $prefix = Config(sprintf('redis.%s.prefix', $this->instance), Config('app_name'));
        return sprintf(
            '%s:tairdoc:%s',
            $prefix,
            $this->key
        );
    }

    public function getKey(): string {
        return $this->key;
    }
}