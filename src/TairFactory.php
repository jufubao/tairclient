<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace TairClient;

use Hyperf\Redis\RedisFactory;
use Hyperf\Utils\ApplicationContext;

class TairFactory
{
    /**
     * @var TairClient[]
     */
    protected array $clients = [];

    public function __construct()
    {

    }

    public function get(string $instance): TairClient
    {
        $client = $this->clients[$instance] ?? null;
        if (! $client instanceof TairClient) {
            $container = ApplicationContext::getContainer();
            $redisClient = $container->get(RedisFactory::class)->get($instance);
            $client = new TairClient($redisClient);
        }
        return $client;
    }
}
